import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";


export const IngredientsValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const ingredientsPart: string[] = control.value.trim().split(';')
    const ingredientsStr:string = control.value.trim();  
    const ingredientAll: string[] = [];

    for(let ingredient of ingredientsPart){
        if(ingredient.split(',').length != 2){
            return {
                ingredientsValidatorError: {
                    message: "Ingredient needs to have name and measure devided with ' , '."
                }
            }
        }
        const tmp:string[]=ingredient.split(',');
        tmp.forEach(function(str){
            if(str.length>0){
            ingredientAll.push(str);  
            }
        })
         
    }

    if (ingredientsPart.length === 0) {
        return {
            ingredientsValidatorError: {
                message: "Recipe must have minimum one ingredient."
            }
        }
    }

    if(ingredientAll.length !== ingredientsPart.length * 2 ){
        return {
            ingredientsValidatorError: {
                message: "Ingredient needs to have name and measure devided with ' , ' . Every ingredient need to be devided with ' ; ' ."
            }
        }
    }
   

    return null;
}
