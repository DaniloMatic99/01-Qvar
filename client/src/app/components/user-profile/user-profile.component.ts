import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user.model';
import { Subscription } from 'rxjs';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit{
 

  user: User | null = null;
  userSub: Subscription = new Subscription();
  userForm: FormGroup;
  shouldDisplayUserForm: boolean = false; 
  private image: File | null = null;

  constructor(private formBuilder: FormBuilder, private auth: AuthService, private userService: UserService) {

    this.userForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(2)]),
      surname: new FormControl('', [Validators.required, Validators.minLength(2)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      imgUrl: new FormControl('')
    });

    this.userSub = this.auth.user.subscribe((user: User | null) => {this.user = user;  });
    this.auth.sendUserDataIfExists();

  }

  ngOnInit(): void {
    
  }

  ngOnDestroy(): void {
    if(this.userSub) {
      this.userSub.unsubscribe();
    }
  }

  onChangeInfo() {
    this.shouldDisplayUserForm = true;
  }

  onSaveChanges() {
    this.shouldDisplayUserForm = false;
  }

  onUserFormSubmit() {
    if (this.userForm.invalid) {
      window.alert('Form is not valid!');
      return;
    } 

    const data = this.userForm.value;

    this.userService.patchUserData(this.user!.username, data.name, data.surname, data.email ).subscribe((user: User) => {
      if(!this.user) {
        this.user = new User('','','','','');
      }
      this.user.username = user.username;
      this.user.name = user.name;
      this.user.surname = user.surname;
      this.user.email = user.email;

      if (this.image !== null) {
          this.userService.patchUserProfileImage(this.image).subscribe((user: User) => {
            this.user!.imgUrl = user.imgUrl;
        });
      }

      this.onSaveChanges();
    });
  }

  

  getProfilePicture(): string {
    if(!this.user || this.user.imgUrl === undefined || this.user.getImageUrl()==="")  {
      return 'assets/cooking.png';
    }
    return this.user.getImageUrl();
  }

  public onFileChange(event: Event): void {
    const files: FileList | null = (event.target as HTMLInputElement).files;
    if(!files || files.length === 0) {
        this.image = null;
        return;
    }
    this.image = files[0];
  }
}