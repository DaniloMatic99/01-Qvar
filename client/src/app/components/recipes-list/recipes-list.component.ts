import { Component, Input, OnInit } from '@angular/core';
import { Recipe } from '../../models/recipe.model';
import { Observable, switchMap } from 'rxjs';
import { RecipeService } from 'src/app/services/recipe.service';
import {RecipePagination} from "../../models/recipe-pagination"
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-recipes-list',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.css']
})
export class RecipesListComponent implements OnInit {

  pagination: Observable<RecipePagination>;

  constructor(private recipeService: RecipeService, private activatedRoute: ActivatedRoute) {
    this.pagination = this.activatedRoute.queryParamMap.pipe(
      switchMap((queryMap: ParamMap) => {
        const page = queryMap.get("page");
        const pageNum: number = page != null ? Number.parseInt(page) : 1;
        return this.recipeService.getRecipes(pageNum, 10);
      }),
    );
  }


  ngOnInit(): void{

  }

}
