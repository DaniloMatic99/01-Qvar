import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-navigation-menu',
  templateUrl: './navigation-menu.component.html',
  styleUrls: ['./navigation-menu.component.css']
})
export class NavigationMenuComponent {
  public user: User | null = null;
  private userSub: Subscription;
  searchForm: FormGroup;
  public strMeal: string | undefined;

  constructor(private auth: AuthService,
              private router: Router,
              private route: ActivatedRoute) {
    this.searchForm = new FormGroup({
      search: new FormControl('',[])
    });
    this.userSub = this.auth.user.subscribe((user: User | null) => {
      this.user = user;
    });
    this.auth.sendUserDataIfExists();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if(this.userSub) {
      this.userSub.unsubscribe();
    }
  }

  public getLoginOrLogoutRouterLink(): string {
    return this.user ? '/logout' : '/login';
  }

  public getLoginOrLogoutString(): string {
    return this.user ? 'Logout' : 'Login';
  }
}
