import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Observable, Subscription, async, isEmpty, isObservable, catchError, of, tap } from 'rxjs';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit,OnDestroy{
  sub: Subscription = new Subscription();
  loginForm: FormGroup;

  errorMessage: string;

  constructor(private auth: AuthService,
              private router: Router) {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.pattern(/^[0-9a-zA-Z_-]{3,}$/)]),
      password: new FormControl('', [Validators.required, Validators.pattern(/^[0-9a-zA-Z_!@#$%^&*()+=-]{2,}$/)]),
    });
    this.errorMessage = "";
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  invalidInputError(): string | null {
    let errors: ValidationErrors | null | undefined = this.loginForm.get('username')?.errors;

    if (errors != null) {
      if (errors['required']) {
        return 'Username must be provided.';
      }
      return 'Invalid username.';
    }

    errors = this.loginForm.get('password')?.errors;
    if (errors != null) {
      if (errors['required']) {
        return 'Password must be provided.';
      }
      return 'Invalid password.';
    }

    // Validation check passed
    return null;
  }

  login(): void {
    this.errorMessage = '';
    if (this.loginForm.invalid) {
      this.errorMessage = this.invalidInputError()!;
      return;
    }

    const data = this.loginForm.value;
    const obs: Observable<User | null | string> = this.auth.login(data.username, data.password);

    this.sub = obs.subscribe((user: User | null | string) => {
      if(typeof(user) == "string") {
          this.errorMessage += user;
          return;
      }
      this.router.navigateByUrl('/');
    });
    
    this.auth.login(data.username, data.password); 
  }
}
