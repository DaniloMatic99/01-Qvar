import { Component } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent {
  registerForm: FormGroup;
  sub: Subscription = new Subscription();

  constructor(private auth: AuthService,
              private router: Router) {
    this.registerForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.pattern(/^[0-9a-zA-Z_-]{5,}$/), Validators.minLength(5)]),
      password: new FormControl('', [Validators.required, Validators.pattern(/^[0-9a-zA-Z_!@#$%^&*()+=-]{4,}$/)]),
      name: new FormControl('', [Validators.required]),
      surname: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
    });
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  ngOnInit(): void {
  }

  register(): void {
    if(this.registerForm.invalid) {
      window.alert("Not valid!");
      return;
    }
    
    const data = this.registerForm.value;
    const obs: Observable<User | null> = this.auth.register(data.username, data.password,data.name, data.surname,  data.email);

    this.sub = obs.subscribe((user: User | null) => {
      this.router.navigateByUrl('/');
    });
  }

  usernameHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.registerForm.get('username')?.errors;
    return errors !== null;
  }

  usernameErrors(): string[] {
    const errors: ValidationErrors | null | undefined = this.registerForm.get('username')?.errors;

    if (errors == null) {
      return [];
    }

    const errorMsgs: string[] = [];

    if (errors['required']) {
      errorMsgs.push('User must have an username.');
    }

    if (errors['minlength']) {
      errorMsgs.push(`Username should be minimum 5 characters long.`);
    }
    return errorMsgs;
  }
}
