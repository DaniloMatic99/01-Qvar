const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwtUtil = require('../utils/jwt');

const SALT_ROUNDS = 10;



const userSchema = new mongoose.Schema({
  name: {
      type: String,
      required: true
  },
  surname: {
      type: String,
      required: true
  },
  username: {
      type: String,
      required: true
  },
  hash: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
  salt: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
  email: {
      type: String,
      required: true
  },
  imgUrl: {
    type: mongoose.Schema.Types.String,
    required: false
  },
  saved_recipes: {
      type: [String],
      default: []
  }
});



userSchema.methods.setPassword = async function (password) {
    this.salt = await bcrypt.genSalt(SALT_ROUNDS);
    this.hash = await bcrypt.hash(password, this.salt);;
};

userSchema.methods.isValidPassword = async function (password) {
    return await bcrypt.compare(password, this.hash);
};
  
const User = mongoose.model('User', userSchema);
  
async function getUserByUsername(username) {
    const user = await User.findOne({ username }).exec();
    return user;
}
  

async function getUserJWTByUsername(username) {
    const user = await getUserByUsername(username);
    if (!user) {
      throw new Error(`User with username ${username} does not exist!`);
    }
    return jwtUtil.generateJWT({
      _id: user._id,
      username: user.username,
      email: user.email,
      name: user.name,
      surname: user.surname,
      imgUrl: user.imgUrl,
    });
}
  

async function registerNewUser(username, password, email, name, surname) {
    const user = new User();
    user.username = username;
    await user.setPassword(password);
    user.email = email;
    user.name = name;
    user.surname = surname;

  
    await user.save();
    return getUserJWTByUsername(username);
}


async function updateUserData(username, name, email,surname) {
    const user = await getUserByUsername(username);
    user.name = name;
    user.email = email;
    user.surname = surname;

    await user.save();
    return getUserJWTByUsername(username);
  }

async function changeUserProfileImage(username,imgUrl) {
    const user = await getUserByUsername(username);
    user.imgUrl = imgUrl;
    await user.save();
}
   

module.exports = {
    getUserByUsername,
    getUserJWTByUsername,
    registerNewUser,
    changeUserProfileImage,
    updateUserData
  };
  