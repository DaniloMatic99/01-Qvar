const express = require('express');
const controller = require('./usersController');

const router = express.Router();


const authentication = require('../utils/authentication');

//postavljanje ruta
router.post('/register', controller.registerUser);
router.post('/login', authentication.canAuthenticate, controller.loginUser);
router.patch('/', authentication.isAuthenticated, controller.changeUserInfoData);
router.patch('/profile-image', authentication.isAuthenticated, controller.changeProfileImage);

module.exports = router;


